"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.pixelsFromImg = pixelsFromImg;
exports.random = random;
exports.mapToImgBounds = mapToImgBounds;
exports.getBlackXys = exports.arrOfN = void 0;

var _getPixels = _interopRequireDefault(require("get-pixels"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var arrOfN = function arrOfN(n) {
  return Array(n).fill(0).map(function (e) {
    return Math.random(0, 100);
  });
};

exports.arrOfN = arrOfN;

var getBlackXys = function getBlackXys(pixels) {
  var xs = arrOfN(pixels.shape[0]);
  var ys = arrOfN(pixels.shape[1]);
  var xys = [];
  xs.forEach(function (r, x) {
    ys.forEach(function (c, y) {
      var r = pixels.get(x, y, 0);
      var g = pixels.get(x, y, 1);
      var b = pixels.get(x, y, 2);

      if (![r, g, b].every(function (f) {
        return f === 255;
      })) {
        xys.push({
          x: x,
          y: y
        });
      }
    });
  });
  return xys;
};

exports.getBlackXys = getBlackXys;

function pixelsFromImg(_x) {
  return _pixelsFromImg.apply(this, arguments);
}

function _pixelsFromImg() {
  _pixelsFromImg = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(path) {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt("return", new Promise(function (res, rej) {
              (0, _getPixels.default)(path, function (err, pixels) {
                if (err) {
                  console.error(err);
                  return rej(err);
                }

                return res(pixels);
              });
            }));

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _pixelsFromImg.apply(this, arguments);
}

function random(min, max) {
  return Math.random() * (max - min) + min;
}

function mapToImgBounds(n, start1, stop1, start2, stop2, withinBounds) {
  var newval = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;

  if (!withinBounds) {
    return newval;
  }

  if (start2 < stop2) {
    return this.constrain(newval, start2, stop2);
  } else {
    return this.constrain(newval, stop2, start2);
  }
}
//# sourceMappingURL=utils.js.map