"use strict";

var _slopepredict = _interopRequireDefault(require("./slopepredict"));

require("@babel/polyfill");

var _events = _interopRequireDefault(require("events"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var sp = new _slopepredict.default();

var runimg =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(it, nam) {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            sp.run(it);
            _context.next = 3;
            return sp.createPng("".concat(it, "-epoch-").concat(nam));

          case 3:
            console.log("".concat(it, "-epoch-").concat(nam));

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function runimg(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

function runit() {
  return _runit.apply(this, arguments);
}

function _runit() {
  _runit = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2() {
    var i;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return sp.setup();

          case 2:
            i = 1;

          case 3:
            if (!(i < 50)) {
              _context2.next = 9;
              break;
            }

            _context2.next = 6;
            return runimg(10 * i, '-');

          case 6:
            i++;
            _context2.next = 3;
            break;

          case 9:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _runit.apply(this, arguments);
}

if (require.main === module) {
  runit();
}
//# sourceMappingURL=index.js.map