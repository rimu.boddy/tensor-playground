"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var tf = _interopRequireWildcard(require("@tensorflow/tfjs-node"));

var _fs = _interopRequireDefault(require("fs"));

var _pngjsDraw = _interopRequireDefault(require("pngjs-draw"));

var _pngjs = require("pngjs");

var _utils = require("./utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var DATA_PATH = './data/';
var IN_IMG = 'dotslope.png';
var png = (0, _pngjsDraw.default)(_pngjs.PNG);
var learningRate = 0.019;
var optimizer = tf.train.adam(learningRate);
var optimizer2 = tf.train.adam(learningRate * 1.2);
var optimizer3 = tf.train.adam(learningRate * 1.3);
var optimizer4 = tf.train.adam(learningRate * 1.4);
var a, b, c, d;
var a1, b2, c3, d4;
var aw, bw, cw, dw;
var aw2, bw2, cw2, dw2;
var dragging = false;

function loss(pred, labels) {
  return pred.sub(labels).square().mean();
}

function predict(x) {
  var xs = tf.tensor1d(x); // y = ax^3 + bx^2 + cx + d
  // y = (a*x to the power of 3) + (b*x squared) + c*X + d;

  var ys = xs.pow(tf.scalar(3)).mul(a).add(xs.square().mul(b)).add(xs.mul(c)).add(d);
  return ys;
}

function predictXY(yx) {
  var ys = tf.tensor1d(yx); // y = ax^3 + bx^2 + cx + d
  // y = (a*x to the power of 3) + (b*x squared) + c*X + d;

  var xs = ys.pow(tf.scalar(3)).mul(aw).add(ys.square().mul(bw)).add(ys.mul(cw)).add(dw);
  return xs;
}

function predictYX(yx) {
  var ys = tf.tensor1d(yx); // y = ax^3 + bx^2 + cx + d
  // y = (a*x to the power of 3) + (b*x squared) + c*X + d;

  var xs = ys.pow(tf.scalar(3)).mul(aw2).add(ys.square().mul(bw2)).add(ys.mul(cw2)).add(dw2);
  return xs;
}

var SlopePredict = function SlopePredict() {
  var _this = this;

  _classCallCheck(this, SlopePredict);

  _defineProperty(this, "xys", []);

  _defineProperty(this, "line", []);

  _defineProperty(this, "linex", []);

  _defineProperty(this, "liney", []);

  _defineProperty(this, "linem", []);

  _defineProperty(this, "imgPixels", null);

  _defineProperty(this, "setup",
  /*#__PURE__*/
  function () {
    var _ref = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(n, y, x) {
      var xys, marks, totalX, totalY;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return (0, _utils.pixelsFromImg)(DATA_PATH + IN_IMG);

            case 2:
              xys = _context.sent;
              _this.xys = xys;
              marks = (0, _utils.getBlackXys)(xys);
              totalX = _this.xys.shape[0];
              totalY = _this.xys.shape[1];
              _this.epochs = 0;
              _this.ys = marks.map(function (_ref2) {
                var y = _ref2.y;
                return (0, _utils.mapToImgBounds)(y, 0, totalY, 1, -1);
              });
              _this.xs = marks.map(function (_ref3) {
                var x = _ref3.x;
                return (0, _utils.mapToImgBounds)(x, 0, totalX, -1, 1);
              });
              a = tf.variable(tf.scalar((0, _utils.random)(-1, 1)));
              b = tf.variable(tf.scalar((0, _utils.random)(-1, 1)));
              c = tf.variable(tf.scalar((0, _utils.random)(-1, 1)));
              d = tf.variable(tf.scalar((0, _utils.random)(-1, 1)));
              a1 = tf.variable(tf.scalar((0, _utils.random)(-1, 1.1)));
              b2 = tf.variable(tf.scalar((0, _utils.random)(-1, 1)));
              c3 = tf.variable(tf.scalar((0, _utils.random)(-1, 1)));
              d4 = tf.variable(tf.scalar((0, _utils.random)(-1, 1.9)));
              aw = tf.variable(tf.scalar((0, _utils.random)(-0.6, 0.6)));
              bw = tf.variable(tf.scalar((0, _utils.random)(-0.6, 0.6)));
              cw = tf.variable(tf.scalar((0, _utils.random)(-0.6, 0.6)));
              dw = tf.variable(tf.scalar((0, _utils.random)(-0.6, 0.6)));
              aw2 = tf.variable(tf.scalar((0, _utils.random)(-1.1, 1)));
              bw2 = tf.variable(tf.scalar((0, _utils.random)(-1.2, 2)));
              cw2 = tf.variable(tf.scalar((0, _utils.random)(-1.3, 1)));
              dw2 = tf.variable(tf.scalar((0, _utils.random)(-1, 1)));

            case 26:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x, _x2, _x3) {
      return _ref.apply(this, arguments);
    };
  }());

  _defineProperty(this, "run", function (iterations) {
    _this.epochs += iterations;

    for (var i = 0; i < iterations; i++) {
      _this.iterate(i);
    }

    ;
  });

  _defineProperty(this, "iterate", function (i) {
    var tys = _this.ys;
    var txs = _this.xs;
    var totalX = _this.xys.shape[0];
    var totalY = _this.xys.shape[1];
    tf.tidy(function () {
      var ys = tf.tensor1d(tys);
      var xs = tf.tensor1d(txs);
      optimizer.minimize(function () {
        return loss(predict(tys), xs);
      });
      optimizer4.minimize(function () {
        return loss(predictXY(xs.dataSync()), ys);
      });
      optimizer3.minimize(function () {
        return loss(predictYX(ys.dataSync()), xs);
      });
      optimizer4.minimize(function () {
        return loss(predictXY(xs.dataSync()), ys);
      });
      optimizer3.minimize(function () {
        return loss(predictYX(ys.dataSync()), xs);
      });
    });
    var curveX = [];
    var curveY = [];

    for (var x = -1; x <= 1; x += 0.025) {
      curveX.push(x);
      curveY.push(x);
    }

    var xs = tf.tidy(function () {
      return predict(curveY);
    });
    var xm = tf.tidy(function () {
      return predict(predictXY(xs.dataSync()).dataSync());
    });
    var ys = tf.tidy(function () {
      return predict(curveX);
    });
    var ym = tf.tidy(function () {
      return predict(predictYX(ys.dataSync()).dataSync());
    });
    var curvey = ys.dataSync();
    var curvex = xs.dataSync();
    var curveym = ym.dataSync();
    var curvexm = xm.dataSync();
    ys.dispose();
    xs.dispose();
    ym.dispose();
    xm.dispose();

    for (var _i = 0; _i < curveX.length; _i++) {
      var _x4 = Math.round((0, _utils.mapToImgBounds)(curveX[_i], -1, 1, 0, totalX));

      var y = Math.round((0, _utils.mapToImgBounds)(curvey[_i], -1, 1, totalY, 0));
      var x2 = Math.round((0, _utils.mapToImgBounds)(curvex[_i], -1, 1, 0, totalX));
      var y2 = Math.round((0, _utils.mapToImgBounds)(curveY[_i], -1, 1, totalY, 0));
      var x3 = Math.round((0, _utils.mapToImgBounds)(curvex[_i], -1, 1, 0, totalX));
      var y3 = Math.round((0, _utils.mapToImgBounds)(curvey[_i], -1, 1, totalY, 0));
      var x3w = Math.round((0, _utils.mapToImgBounds)(curvexm[_i], -1, 1, 0, totalX));
      var y3w = Math.round((0, _utils.mapToImgBounds)(curveym[_i], -1, 1, totalY, 0));
      _this.line[_i] = {
        x: _x4,
        y: y
      };
      _this.linex[_i] = {
        x: x2,
        y: y2
      };
      _this.liney[_i] = {
        x: x3,
        y: y3
      };
      _this.linem[_i] = {
        x: x3w,
        y: y3w
      };
    }
  });

  _defineProperty(this, "createPng", function () {
    var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'img.png';
    var line = _this.line;
    var linex = _this.linex;
    var linem = _this.linem;
    var liney = _this.liney;
    return new Promise(function (res, rej) {
      _fs.default.createReadStream(DATA_PATH + IN_IMG).pipe(new png({
        filterType: 4
      })).on('parsed', function () {
        // Draws a pixel with transparent green
        //this.drawPixel(150,200, this.colors.black())
        // Draws a line with transparent red
        for (var i = 1; i < line.length; i++) {
          var p1 = line[i - 1];
          var p2 = line[i];
          this.drawLine(p1.x, p1.y, p2.x, p2.y, this.colors.red(150));
        }

        for (var _i2 = 1; _i2 < linex.length; _i2++) {
          var _p = linex[_i2 - 1];
          var _p2 = linex[_i2];
          this.drawLine(_p.x, _p.y, _p2.x, _p2.y, this.colors.green(150));
        }

        for (var _i3 = 1; _i3 < liney.length; _i3++) {
          var _p3 = liney[_i3 - 1];
          var _p4 = liney[_i3];
          this.drawLine(_p3.x, _p3.y, _p4.x, _p4.y, this.colors.blue(150));
        }

        for (var _i4 = 1; _i4 < linem.length; _i4++) {
          var _p5 = linem[_i4 - 1];
          var _p6 = linem[_i4];
          this.drawLine(_p5.x, _p5.y, _p6.x, _p6.y, this.colors.new(170, 190, 70));
        } // Draws a rectangle with transparent black
        //this.fillRect(150,150,75,20, this.colors.black(100))
        // Draws a filled rectangle with transparent white
        //this.fillRect(50,50,100,100, this.colors.white(100))
        // Draws a text with custom color


        for (var _i5 = 1; _i5 < line.length; _i5++) {
          this.drawText(line[_i5].x, line[_i5].y, "X", this.colors.new(256, 90, 50));
        } // Writes file


        var ws = this.pack().pipe(_fs.default.createWriteStream('/Users/eviangela/Projects/img/dots-out-' + name));
        ws.on('finish', function () {
          return res(line);
        });
      });
    });
  });
};

exports.default = SlopePredict;
//# sourceMappingURL=slopepredict.js.map