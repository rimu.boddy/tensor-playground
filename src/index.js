import SlopePredict from './slopepredict';

import '@babel/polyfill';
import EventEmitter from 'events';

const sp = new SlopePredict();

const runimg = async function(it, nam) {
  sp.run(it);
  await sp.createPng(`${it}-epoch-${nam}`);
  console.log(`${it}-epoch-${nam}`);
}

async function runit() {
  await sp.setup();
  for(var i = 1; i < 50; i ++) {
    await runimg(10 * i, '-');
  }
}

if (require.main === module) {
 runit();
}
