import * as tf from '@tensorflow/tfjs-node';
import fs from 'fs';
import drawing from 'pngjs-draw';
import { PNG } from 'pngjs';
import { pixelsFromImg, random, mapToImgBounds, getBlackXys } from './utils';

const DATA_PATH = './data/';
const IN_IMG = 'dotslope.png';
const png = drawing(PNG);

const learningRate = 0.019;
const optimizer = tf.train.adam(learningRate);
const optimizer2 = tf.train.adam(learningRate*1.2);
const optimizer3 = tf.train.adam(learningRate*1.3);
const optimizer4 = tf.train.adam(learningRate*1.4);


let a, b, c, d;
let a1, b2, c3, d4;
let aw, bw, cw, dw;
let aw2, bw2, cw2, dw2;
let dragging = false;

function loss(pred, labels) {
  return pred.sub(labels).square().mean();
}

function predict(x) {
  const xs = tf.tensor1d(x);
  // y = ax^3 + bx^2 + cx + d
  // y = (a*x to the power of 3) + (b*x squared) + c*X + d;
  const ys = xs.pow(tf.scalar(3)).mul(a)
    .add(xs.square().mul(b))
    .add(xs.mul(c))
    .add(d);
  return ys;
}

function predictXY(yx) {
  const ys = tf.tensor1d(yx);
  // y = ax^3 + bx^2 + cx + d
  // y = (a*x to the power of 3) + (b*x squared) + c*X + d;
  const xs = ys.pow(tf.scalar(3)).mul(aw)
    .add(ys.square().mul(bw))
    .add(ys.mul(cw))
    .add(dw);

  return xs;
}

function predictYX(yx) {
  const ys = tf.tensor1d(yx);
  // y = ax^3 + bx^2 + cx + d
  // y = (a*x to the power of 3) + (b*x squared) + c*X + d;
  const xs = ys.pow(tf.scalar(3)).mul(aw2)
  .add(ys.square().mul(bw2))
  .add(ys.mul(cw2))
  .add(dw2);

  return xs;
}


export default class SlopePredict {

  xys = [];
  line = [];
  linex = [];
  liney = [];
  linem = [];
  imgPixels = null;
  
  setup = async (n, y, x) => {
    const xys = await pixelsFromImg(DATA_PATH+IN_IMG);
    this.xys = xys;
    const marks = getBlackXys(xys);
    const totalX = this.xys.shape[0];
    const totalY = this.xys.shape[1];
    this.epochs = 0;
    
    this.ys = marks.map(({ y }) => mapToImgBounds(y, 0, totalY, 1, -1));
    this.xs = marks.map(({ x }) => mapToImgBounds(x, 0, totalX, -1, 1));

    a = tf.variable(tf.scalar(random(-1, 1)));
    b = tf.variable(tf.scalar(random(-1, 1)));
    c = tf.variable(tf.scalar(random(-1, 1)));
    d = tf.variable(tf.scalar(random(-1, 1)));

    a1 = tf.variable(tf.scalar(random(-1, 1.1)));
    b2 = tf.variable(tf.scalar(random(-1, 1)));
    c3 = tf.variable(tf.scalar(random(-1, 1)));
    d4 = tf.variable(tf.scalar(random(-1, 1.9)));

    aw = tf.variable(tf.scalar(random(-0.6, 0.6)));
    bw = tf.variable(tf.scalar(random(-0.6, 0.6)));
    cw = tf.variable(tf.scalar(random(-0.6, 0.6)));
    dw = tf.variable(tf.scalar(random(-0.6, 0.6)));

    aw2 = tf.variable(tf.scalar(random(-1.1, 1)));
    bw2 = tf.variable(tf.scalar(random(-1.2, 2)));
    cw2 = tf.variable(tf.scalar(random(-1.3, 1)));
    dw2 = tf.variable(tf.scalar(random(-1, 1)));
  }

  run = (iterations) => {
    this.epochs += iterations;
    for(var i = 0; i < iterations; i++) {
      this.iterate(i);
    };
  }

  iterate = (i) => {
    const tys = this.ys;
    const txs = this.xs;
    const totalX = this.xys.shape[0];
    const totalY = this.xys.shape[1];

    tf.tidy(() => {
      const ys = tf.tensor1d(tys);
      const xs = tf.tensor1d(txs);
      optimizer.minimize(() => loss(predict(tys), xs));
      optimizer4.minimize(() => loss(predictXY(xs.dataSync()), ys));
      optimizer3.minimize(() => loss(predictYX(ys.dataSync()), xs));
      optimizer4.minimize(() => loss(predictXY(xs.dataSync()), ys));
      optimizer3.minimize(() => loss(predictYX(ys.dataSync()), xs));
    });


    const curveX = [];
    const curveY = [];
    for (let x = -1; x <= 1; x += 0.025) {
      curveX.push(x);
      curveY.push(x);
    }

    const xs = tf.tidy(() => predict(curveY));
    const xm = tf.tidy(() => predict(predictXY(xs.dataSync()).dataSync()));
    
    const ys = tf.tidy(() => predict(curveX));
    const ym = tf.tidy(() => predict(predictYX(ys.dataSync()).dataSync()));

    let curvey = ys.dataSync();
    let curvex = xs.dataSync();
    let curveym = ym.dataSync();
    let curvexm = xm.dataSync();
    
    ys.dispose();
    xs.dispose();
    ym.dispose();
    xm.dispose();

    for (let i = 0; i < curveX.length; i++) {
      let x = Math.round(mapToImgBounds(curveX[i], -1, 1, 0, totalX));
      let y = Math.round(mapToImgBounds(curvey[i], -1, 1, totalY, 0));
      
      let x2 = Math.round(mapToImgBounds(curvex[i], -1, 1, 0, totalX));
      let y2 = Math.round(mapToImgBounds(curveY[i], -1, 1, totalY, 0));

      let x3 = Math.round(mapToImgBounds(curvex[i], -1, 1, 0, totalX));
      let y3 = Math.round(mapToImgBounds(curvey[i], -1, 1, totalY, 0));

      let x3w = Math.round(mapToImgBounds(curvexm[i], -1, 1, 0, totalX));
      let y3w = Math.round(mapToImgBounds(curveym[i], -1, 1, totalY, 0));

      this.line[i] = { x:x, y:y };
      this.linex[i] = { x:x2, y:y2 };
      this.liney[i] = { x:x3, y:y3 };
      this.linem[i] = { x:x3w, y:y3w };
    }
  }

  createPng = (name='img.png') => {
    const line = this.line;
    const linex = this.linex;
    const linem = this.linem;
    const liney = this.liney;
    return new Promise((res, rej) => {
      fs.createReadStream(DATA_PATH+IN_IMG)
      .pipe(new png({ filterType: 4 }))
      .on('parsed', function() {
        // Draws a pixel with transparent green
        //this.drawPixel(150,200, this.colors.black())
    
        // Draws a line with transparent red
        for (let i = 1; i < line.length; i ++) {
          const p1 = line[i-1];
          const p2 = line[i];
          this.drawLine(p1.x,p1.y,p2.x,p2.y, this.colors.red(150))
        }

        for (let i = 1; i < linex.length; i ++) {
          const p1 = linex[i-1];
          const p2 = linex[i];
          this.drawLine(p1.x,p1.y,p2.x,p2.y, this.colors.green(150))
        }
        for (let i = 1; i < liney.length; i ++) {
          const p1 = liney[i-1];
          const p2 = liney[i];
          this.drawLine(p1.x,p1.y,p2.x,p2.y, this.colors.blue(150))
        }
        for (let i = 1; i < linem.length; i ++) {
          const p1 = linem[i-1];
          const p2 = linem[i];
          this.drawLine(p1.x,p1.y,p2.x,p2.y, this.colors.new(170,190,70));
        }
    
        // Draws a rectangle with transparent black
        //this.fillRect(150,150,75,20, this.colors.black(100))
    
        // Draws a filled rectangle with transparent white
        //this.fillRect(50,50,100,100, this.colors.white(100))
    
        // Draws a text with custom color
        for (let i = 1; i < line.length; i ++) {
          this.drawText(line[i].x,line[i].y, "X", this.colors.new(256,90,50));
        }
        // Writes file
        const ws = this.pack().pipe(fs.createWriteStream('/Users/eviangela/Projects/img/dots-out-' + name));
        ws.on('finish', () => res(line));
      });
    }); 
  }
}
