import getPixels from 'get-pixels';


export const arrOfN = (n) => {
  return Array(n).fill(0).map(e => Math.random(0, 100));
}

export const getBlackXys = (pixels) => {
  const xs = arrOfN(pixels.shape[0]);
  const ys = arrOfN(pixels.shape[1]);
  const xys = [];
  xs.forEach((r, x) => {
    ys.forEach((c, y) => {
      const r = pixels.get(x, y, 0);
      const g = pixels.get(x, y, 1);
      const b = pixels.get(x, y, 2);
      if(![r,g,b].every(f => f === 255)) {
        xys.push({ x, y });
      }
    });
  });
  return xys;
}

export async function pixelsFromImg(path) {
  return new Promise((res, rej) => {
    getPixels(path, (err, pixels) => {
      if(err) {
        console.error(err);
        return rej(err);
      }
      return res(pixels);
    });
  });
}

export function random(min, max) {
  return Math.random() * (max - min) + min;
}

export function mapToImgBounds(n, start1, stop1, start2, stop2, withinBounds) {
  var newval = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
  if (!withinBounds) {
    return newval;
  }
  if (start2 < stop2) {
    return this.constrain(newval, start2, stop2);
  } else {
    return this.constrain(newval, stop2, start2);
  }
}
